<?php

namespace App\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use PDOException;
use PDO;
use Faker;

class SaySeedCommand extends Command
{
    protected function configure()
    {
        $this->setName('seed:users')
            ->setDescription("This command allows you generate users with Faker")
            ->setHelp('This command will seed users.')
            ->addOption('count', 'count', InputOption::VALUE_REQUIRED, 'How many times do you want to input the repeat', 1);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $pdo = new PDO("mysql:host=localhost;dbname=users", "root", "");
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $fakerPerson = Faker\Factory::create('fr_FR');

            $progressBar = new ProgressBar($output, $input->getOption('count'));
            $progressBar->start();

            for ($i = 0; $i < $input->getOption('count'); $i++) {
                $progressBar->setMessage($i, 'item'); // set the `item` value
                $progressBar->advance();
                usleep(1000);
                $query = "INSERT INTO users (`first_name`, `last_name`, `email`) VALUES ('{$fakerPerson->firstNameMale}', '{$fakerPerson->lastName}', '{$fakerPerson->email}')";
                $q = $pdo->prepare($query);
                $q->execute();
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}